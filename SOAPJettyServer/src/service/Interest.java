
package service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="startSum" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="interest" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="years" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "startSum",
    "interest",
    "years"
})
@XmlRootElement(name = "interest")
public class Interest {

    protected double startSum;
    protected double interest;
    protected int years;

    /**
     * Gets the value of the startSum property.
     * 
     */
    public double getStartSum() {
        return startSum;
    }

    /**
     * Sets the value of the startSum property.
     * 
     */
    public void setStartSum(double value) {
        this.startSum = value;
    }

    /**
     * Gets the value of the interest property.
     * 
     */
    public double getInterest() {
        return interest;
    }

    /**
     * Sets the value of the interest property.
     * 
     */
    public void setInterest(double value) {
        this.interest = value;
    }

    /**
     * Gets the value of the years property.
     * 
     */
    public int getYears() {
        return years;
    }

    /**
     * Sets the value of the years property.
     * 
     */
    public void setYears(int value) {
        this.years = value;
    }

}
