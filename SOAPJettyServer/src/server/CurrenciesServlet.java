package server;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tempuri.ConverterLocator;
import org.tempuri.ConverterSoap;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import service.CalculatorServiceImpl;
import service.CalculatorServiceImplService;

@WebServlet("/currencies")
public class CurrenciesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		

		JsonArray arr = new JsonArray();

		JsonObject jo = new JsonObject();
		try {
			ConverterLocator cl = new ConverterLocator();
			ConverterSoap cs = cl.getConverterSoap();
			String s[] = cs.getCurrencies();
			
			for (String ss: s) {
				arr.add(ss);	
			}
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			jo.addProperty("err", e.getMessage());
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		jo.addProperty("Status", response.getStatus());
		jo.add("currencies", arr);

		response.setContentType("application/json");
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(jo.toString());
		writer.flush();
		writer.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
		writer.write("this is just a POST test");
		writer.flush();
		writer.close();
	}
}
