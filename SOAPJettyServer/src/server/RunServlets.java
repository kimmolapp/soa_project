package server;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;

public class RunServlets {
	public static void main(String[] args) throws Exception {
		ServletHandler handler = new ServletHandler();
		
		handler.addServletWithMapping(AddServlet.class, "/add");
		handler.addServletWithMapping(SubtractServlet.class, "/subtract");
		handler.addServletWithMapping(MultiplyServlet.class, "/multiply");
		handler.addServletWithMapping(DivideServlet.class, "/divide");
		handler.addServletWithMapping(InterestServlet.class, "/interest");
		handler.addServletWithMapping(CurrenciesServlet.class, "/currencies");
		
		Server server = new Server(8081);
		server.setHandler(handler);
		server.start();
		
		server.dumpStdErr();
		server.join();
	}
}
