package server;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.CalculatorServiceImpl;
import service.CalculatorServiceImplService;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String output;
		
		try {
			Double a = Double.parseDouble(request.getParameter("a"));
			Double b = Double.parseDouble(request.getParameter("b"));

			CalculatorServiceImplService calc = new CalculatorServiceImplService();
			CalculatorServiceImpl soap = calc.getCalculatorServiceImpl();

			output = Double.toString(soap.add(a, b));
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			output = e.getMessage();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(output);
		writer.flush();
		writer.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
		writer.write("this is just a POST test");
		writer.flush();
		writer.close();
	}
}
