package server;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.tempuri.Converter;
import org.tempuri.ConverterLocator;
import org.tempuri.ConverterSoap;

import com.dataaccess.webservicesserver.NumberConversion;
import com.dataaccess.webservicesserver.NumberConversionSoapType;

import service.CalculatorServiceImpl;
import service.CalculatorServiceImplService;

@WebServlet("/interest")
public class InterestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		String output;
		
		try {
			Double startSum = Double.parseDouble(request.getParameter("startSum"));
			Double interest = Double.parseDouble(request.getParameter("interest"));
			int years = Integer.parseInt(request.getParameter("years"));
			String currency1 = request.getParameter("currency1");
			String currency2 = request.getParameter("currency2");
			
			CalculatorServiceImplService calc = new CalculatorServiceImplService();
			CalculatorServiceImpl soap = calc.getCalculatorServiceImpl();

			BigDecimal number = new BigDecimal(soap.interest(startSum, interest, years));
			
			
			ConverterLocator cl = new ConverterLocator();
			ConverterSoap cs = cl.getConverterSoap();
			String s[] = cs.getCurrencies();
			
			Calendar c = cs.getLastUpdateDate();
			
			BigDecimal convertedNumber = cs.getConversionAmount(currency1, "usd", c, number);
			//cs.getConversionAmount("usd", currencyTo, rateDate, amount)
				
			
			NumberConversion conversionService = new NumberConversion();
			NumberConversionSoapType conversion = conversionService.getNumberConversionSoap();
			output = conversion.numberToDollars(convertedNumber);
			if (output == null || "".equals(output)) {
				throw new Exception("Conversion service not available");	
			}
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (Exception e) {
			output = e.getMessage();
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.write(output);
		writer.flush();
		writer.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
		writer.write("this is just a POST test");
		writer.flush();
		writer.close();
	}
}
