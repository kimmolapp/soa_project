This is a repository for SOA and Cloud Computing tasks.

Project descriptions

SOAPService
	Simple SOAP service which currently can add, substract and multiply integers.
	Also calculates interest
	usage: run on Tomcat server
	url: http://localhost:8080/SOAPService/services/CalculatorServiceImpl?wsdl

SOAPServiceClient
	Automatically generated client to test the SOAPService
	usage: run on Tomcat server
	url: http://localhost:8080/SOAPServiceClient/sampleCalculatorServiceImplProxy/TestClient.jsp

SOAPJettyServer
	Uses the calculator functions in the SOAPService
	usage: run the main method from RunServlets class as java application 
	configuration: add .jar files from Jetty's lib folder to buid path 
	urls:
		http://localhost:8081/add?a=2&b=3
		http://localhost:8081/subtract?a=2&b=3
		http://localhost:8081/multiply?a=2&b=3
		http://localhost:8081/divide?a=2&b=3
		http://localhost:8081/interest?startSum=100.50&interest=1.5&years=10&currency=USD

MyDBoxClient
	Connects to Dropbox using appKey and appSecretKey from Dropbox app and uploads a file.
	usage: run on Tomcat server
	url: http://localhost:8080/MyDBoxClient

music_catalogue
	Maven project for music aficionados.
	usage: run on Tomcat
	url: http://localhost:8080/music_catalogue/webapi/albums etc.

