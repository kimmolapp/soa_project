package team1soa.storage;

import team1soa.resource.Event;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import team1soa.resource.Event;

@Singleton
public class EventStorage {
	private static Map<Long, Event> events = new HashMap<Long, Event>();

	private static long idCounter = 0;
	
	public static synchronized void addEvent(Event event) {
		event.setId(++idCounter);
		events.put(event.getId(), event);
	}
	

	public static synchronized Event getEvent(Long id) {
		return events.get(id);
	}


	public static synchronized Map<Long, Event> getAllEvents() {
		// TODO Auto-generated method stub
		return events;
	}


	public static synchronized void remove(long id) {
		events.remove(id);
	}


	public static Map<Long, Event> getAllArtistEvents(long artistId) {
		Map<Long, Event> artistEvents = new HashMap<Long, Event>();
		for (Event event : events.values()) {
			if (event.getArtistId() == artistId) {
				artistEvents.put(artistId, event);
			}
		}
		return artistEvents;
	}
}