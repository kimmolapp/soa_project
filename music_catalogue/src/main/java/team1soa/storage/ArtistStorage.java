package team1soa.storage;

import team1soa.resource.Artist;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import team1soa.resource.Artist;

@Singleton
public class ArtistStorage {
	private static Map<Long, Artist> artists = new HashMap<Long, Artist>();

	private static long idCounter = 0;
	
	public static synchronized void addArtist(Artist artist) {
		artist.setId(++idCounter);
		artists.put(artist.getId(), artist);
	}
	

	public static synchronized Artist getArtist(Long id) {
		return artists.get(id);
	}


	public static synchronized Map<Long, Artist> getAllArtists() {
		// TODO Auto-generated method stub
		return artists;
	}


	public static synchronized void remove(long id) {
		artists.remove(id);
	}
}