package team1soa.storage;

import java.util.*;

import javax.inject.Singleton;

import team1soa.exception.DataNotFoundException;
import team1soa.resource.Album;
import team1soa.resource.Artist;

@Singleton
public class AlbumStorage {
	private static Map<Long, Album> storage = new HashMap<Long, Album>();
	private static long id = 0;

	public static synchronized void add(Album album) {
		// TODO: when adding an album, check if it already has an id (for example
		// when album is deserialized from file)
		// if (album.getId() == null) {
		// album.setId(id++);
		// }
		album.setId(id++);
		storage.put(album.getId(), album);

		// Check the ArtistStorage to see if artist exists so we can link album and
		// artist together. Use MusicBrainz id as an identifier.
		// TODO: Create a Map<String, Long> mbids in the ArtistStorage, so MusicBrainz
		// ids can be tracked faster.
		Boolean foundArtist = false;
		if (album.getArtist_id() == null && album.getArtist_mbid() != null) {
			List<Artist> artists = new ArrayList<Artist>(ArtistStorage.getAllArtists().values());
			for (Artist a : artists) {
				if (a.getArtist_mbid() != null && a.getArtist_mbid().equals(album.getArtist_mbid())) {
					album.setArtist_id(a.getId());
					foundArtist = true;
					break;
				}
			}
		}
		// Artist was not found. Create a new artist and add it to ArtistStorage.
		if (!foundArtist) {
			Artist artist = new Artist();
			artist.setArtist_mbid(album.getArtist_mbid());
			artist.setName(album.getArtist());
			ArtistStorage.addArtist(artist);
			album.setArtist_id(artist.getId());
		}

		// Create links to album, artist, and review
		album.addLink("/webapi/albums/" + album.getId(), "self");
		album.addLink("/webapi/albums/" + album.getId() + "/review", "review");
		album.addLink("/webapi/artist/" + album.getArtist_id(), "artist");

	}

	public static synchronized List<Album> getList() {
		return new ArrayList<Album>(storage.values());
	}

	public static synchronized Album get(long id) throws DataNotFoundException {
		if (!storage.containsKey(id)) {
			throw new DataNotFoundException("Album with id " + id + " not found");
		}
		return storage.get(id);
	}

	public synchronized static void remove(long id) throws DataNotFoundException {
		if (!storage.containsKey(id)) {
			throw new DataNotFoundException("Album with id " + id + " not found");
		}
		storage.remove(id);
	}

	public static synchronized void printStorage() {
		for (Map.Entry<Long, Album> pair : storage.entrySet()) {
			Album album = pair.getValue();
			System.out.println(album);
		}
	}

	public static synchronized boolean existAlbum(long id) {
		if (storage.containsKey(id) == false) {
			return false;
		}
		return true;
	}

	/*
	 * public synchronized Album createReview(Review review) { Album z = l.get(id);
	 * z.setReview(review); return z; }
	 */

}
