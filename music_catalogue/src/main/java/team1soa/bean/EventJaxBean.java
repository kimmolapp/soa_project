package team1soa.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class EventJaxBean {
	@XmlElement public long artistId;
	@XmlElement  public long id;
	@XmlElement  public String location;
	@XmlElement  public Date date;
}
