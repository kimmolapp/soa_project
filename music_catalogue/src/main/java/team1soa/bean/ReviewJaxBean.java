package team1soa.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ReviewJaxBean {
	
	@XmlElement public int rating;
	@XmlElement public String review;
	
}
