package team1soa.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AlbumJaxBean {
    @XmlElement public String mbid;
    @XmlElement public String artist;
    @XmlElement public String title;
    @XmlElement public String format;
    @XmlElement public String label;
    @XmlElement public String date;
    @XmlElement public String type;
    @XmlElement public String artist_mbid;
}
