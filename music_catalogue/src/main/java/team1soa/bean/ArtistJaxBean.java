package team1soa.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ArtistJaxBean {
	@XmlElement public String mbid;
	@XmlElement public String name;
	@XmlElement public String country;
	@XmlElement public String disabiguation;
	@XmlElement public Long id;
}
