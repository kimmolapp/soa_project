package team1soa.music_catalogue;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import musicbrainz.MusicBrainzQuery;
import musicbrainz.MusicBrainzQueryResultConverter;
import team1soa.bean.ArtistJaxBean;
import team1soa.bean.EventJaxBean;
import team1soa.exception.DataNotFoundException;
import team1soa.resource.Artist;
import team1soa.resource.Event;
import team1soa.storage.ArtistStorage;
import team1soa.storage.EventStorage;


@Path("artists/{artistId}/events")
public class EventResource {
		@GET
		@Produces(MediaType.APPLICATION_JSON)
		public Response getevents(@PathParam("artistId") long artistId) {
			Artist artist = ArtistStorage.getArtist(artistId);
			if (artist == null) {
				throw new DataNotFoundException("Artist with id " + artistId + " not found");
			}
			Map<Long, Event> events = EventStorage.getAllArtistEvents(artistId);
			if (events == null) {
				throw new DataNotFoundException("List of events is empty");
			}

			
			return Response.status(Status.OK).entity(events).build();
		}

		@GET
		@Path("/{eventId}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getAlbum(@PathParam("eventId") long id, @PathParam("artistId") long artistId) {
			Artist artist = ArtistStorage.getArtist(artistId);
			if (artist == null) {
				throw new DataNotFoundException("Artist with id " + artistId + " not found");
			}
			Event event = EventStorage.getEvent(id);
			if (event == null) {
				throw new DataNotFoundException("Event with id " + id + " not found");
			}
			return Response.status(Status.OK)
					.header(HttpHeaders.LOCATION, "http://localhost:8080/music_catalogue/webapi/events/" + event.getId())
					.entity(event).build();
		}

		@POST
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response createevent(EventJaxBean input, @PathParam("artistId") long artistId) {
			Artist artist = ArtistStorage.getArtist(artistId);
			if (artist == null) {
				throw new DataNotFoundException("Artist with id " + artistId + " not found");
			}
			Event event = new Event();
				event.setArtistId(artistId);
				event.setDate(input.date);
				event.setLocation(input.location);
				EventStorage.addEvent(event);

				event.addLink("/webapi/artists/"+ event.getArtistId() + "/events/" + event.getId(), "self");

				event.addLink("/webapi/artists/" + event.getArtistId(), "artist");
				return Response.status(Status.CREATED)
						.header(HttpHeaders.LOCATION,
								"http://localhost:8080/music_catalogue/webapi/events/" + event.getId())
						.entity(event).build();
			
		}

		@DELETE
		@Path("/{eventId}")
		@Produces(MediaType.APPLICATION_JSON)
		public Response deleteEvent(@PathParam("eventId") long id, @PathParam("artistId") long artistId) {
			Artist artist = ArtistStorage.getArtist(artistId);
			if (artist == null) {
				throw new DataNotFoundException("Artist with id " + artistId + " not found");
			}
			if (EventStorage.getEvent(id) == null) {
				throw new DataNotFoundException("Event with id " + id + " not found");
			}
			EventStorage.remove(id);
			return Response.status(Status.NO_CONTENT).build();
		}

		@PUT
		@Path("/{eventId}")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public Response createReview(EventJaxBean inputEvent, @PathParam("eventId") long id) {
			Event event = EventStorage.getEvent(id);
			if (event == null) {
				throw new DataNotFoundException("Event with id " + id + " not found");
			}
			if (!(event.getDate().equals(inputEvent.date))) {
				event.setDate(inputEvent.date);
			} 
			if (inputEvent.location != null) {
				event.setLocation(inputEvent.location);
			} 

			return Response.status(Status.NO_CONTENT).entity(event).build();
		}
}
