package team1soa.music_catalogue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import musicbrainz.MusicBrainzQuery;
import musicbrainz.MusicBrainzQueryResultConverter;
import team1soa.bean.AlbumJaxBean;
import team1soa.exception.DataNotFoundException;
import team1soa.exception.DataNotFoundExceptionMapper;
import team1soa.resource.Album;
import team1soa.storage.AlbumStorage;

@Path("albums")
public class AlbumResource {
    // private AlbumService albumService = new AlbumService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAlbums() {
        AlbumStorage.printStorage();
        List<Album> albums = AlbumStorage.getList();
        if (albums == null) {
            throw new DataNotFoundException("List of albums is empty");
        }
        return Response.status(Status.OK).entity(albums).build();
    }
    
    @GET
    @Path("/find")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getListAlbum(@Context UriInfo info) {
        List<Album> l = AlbumStorage.getList();
        List<Album> listAlbums = new ArrayList<Album>();
        String type = info.getQueryParameters().getFirst("type");
            for (Album a : l) {
                System.out.println(a.getType());
                if(a.getType() != null && a.getType().equals(type)) {
                    listAlbums.add(a);
                }
            }

        /*if(artist != null){
            for (Album a : l) {
                if(a.getArtist().equals(artist) == true) {
                    listAlbums.add(a);
                }
            }
        }
        System.out.println("type");
        System.out.println(type);
        return listAlbums;
        String type = info.getQueryParameters().getFirst("type");*/
            //System.out.println("type");
            System.out.println(type);
        return Response
                   .status(200)
                   .entity(listAlbums).build();
    }
    
    @GET
    @Path("/{albumId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAlbum(@PathParam("albumId") long id) {
        try {
            Album album = AlbumStorage.get(id);
            return Response.status(Status.OK)
                    .header(HttpHeaders.LOCATION, "http://localhost:8080/music_catalogue/webapi/albums/" + album.getId())
                    .entity(album)
                    .build();
        } catch (DataNotFoundException e) {
            // TODO: where to link after not found
            DataNotFoundExceptionMapper m = new DataNotFoundExceptionMapper();
            return m.toResponse(e);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAlbum(AlbumJaxBean input, @Context UriInfo uriInfo) throws URISyntaxException {
        Album album = new Album();
        Boolean created = false;

        if (input.mbid != null) {
            MusicBrainzQuery mbq = new MusicBrainzQuery(
                    "music_catalogue", 
                    "0.1", 
                    "jukrsund@student.jyu.fi");
            String s = mbq.getAlbumById(input.mbid);
            album = MusicBrainzQueryResultConverter.convertJsonToAlbum(s);
            AlbumStorage.add(album);
            created = true;
            
        } else if (input.artist != null && input.title != null) {
            // No MusicBrainz id was found. Create a new album as long as
            // it contains artist name and title.
            album.setArtist(input.artist);
            album.setTitle(input.title);
            if (input.date != null)
                album.setDate(input.date);
            if (input.artist_mbid != null)
                album.setArtist_mbid(input.artist_mbid);
            if (input.format != null)
                album.setFormat(input.format);
            if (input.label != null)
                album.setLabel(input.label);
            if (input.mbid != null)
                album.setMbid(input.mbid);
            if (input.type != null)
                album.setType(input.type);

            AlbumStorage.add(album);
            created = true;
        }
        if (created) {
            String uriString = uriInfo.getBaseUriBuilder()
                    .path(AlbumResource.class)
                    .path(Long.toString(album.getId()))
                    .toString();
            
            album.addLink(uriString, "absolute_self");
            
            URI uri = new URI(uriString);
            return Response.created(uri)
                    .entity(album)
                    .build();
        }

        // Request did not contain required fields
        return Response.status(Status.BAD_REQUEST)
                .entity("Request body did not contain required fields. 'mbid' or 'artist' and 'title' are required")
                .build();
    }

    @PUT
    @Path("/{albumId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAlbum(AlbumJaxBean input, @PathParam("albumId") long id) {
        Album album;
        try {
            album = AlbumStorage.get(id);
        } catch (DataNotFoundException e) {
            // TODO: where to link after not found
            DataNotFoundExceptionMapper m = new DataNotFoundExceptionMapper();
            return m.toResponse(e);
        }
        // TODO: maybe altering the object should be done in the AlbumStorage...
        // Or maybe we should create a new Album object from the AlbumJaxBean
        // and add it to AlbumStorage
        if (input.artist != null)
            album.setArtist(input.artist);
        if (input.date != null)
            album.setDate(input.date);
        if (input.artist_mbid != null)
            album.setArtist_mbid(input.artist_mbid);
        if (input.format != null)
            album.setFormat(input.format);
        if (input.label != null)
            album.setLabel(input.label);
        if (input.mbid != null)
            album.setMbid(input.mbid);
        if (input.title != null)
            album.setTitle(input.title);
        if (input.type != null)
            album.setType(input.type);

        return Response.status(Status.NO_CONTENT)
                .build();
    }

    @DELETE
    @Path("/{albumId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deletePublication(@PathParam("albumId") long id) {
        try {
            AlbumStorage.remove(id);
            return Response.status(Status.NO_CONTENT)
                    .build();
        } catch (DataNotFoundException e) {
            // TODO: where to link after not found
            DataNotFoundExceptionMapper m = new DataNotFoundExceptionMapper();
            return m.toResponse(e);
        }
    }

}
