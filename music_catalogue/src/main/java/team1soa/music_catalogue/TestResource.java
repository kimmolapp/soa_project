package team1soa.music_catalogue;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import musicbrainz.MusicBrainzQueryResultConverter;
import team1soa.resource.Album;
import team1soa.storage.AlbumStorage;

@Path("test")
public class TestResource {

	@GET
	public Response generateTestData() {
		readFromFile();
		return Response.status(Status.OK).build();
	}

	private static void readFromFile() {
		System.out.println("reading from file");
		try {
			URL url = new URL("http://localhost:8080/music_catalogue/albums/albums.txt");

			Scanner s = new Scanner(url.openStream());
			String content = s.useDelimiter("\\Z").next();
			s.close();
			
			JsonArray arr = new JsonArray();
			JsonParser parser = new JsonParser();
			arr = parser.parse(content).getAsJsonArray();
			
			for (JsonElement elem : arr) {
				JsonObject json = elem.getAsJsonObject();
				Album a = MusicBrainzQueryResultConverter.convertJsonToAlbum(json);
				AlbumStorage.add(a);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
