package team1soa.music_catalogue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import musicbrainz.MusicBrainzQuery;
import musicbrainz.MusicBrainzQueryResultConverter;
import team1soa.resource.Album;
import team1soa.storage.AlbumStorage;

public class AlbumService {
	//private static Map<Long, Album> albums = new HashMap<Long, Album>();
	//public AlbumStorage albumStorage = new AlbumStorage();
	//private static long id = 0;
	
	public AlbumService() {
		String[] albumIds = {
				"5d227193-8f7e-4f6b-b9d1-2faa3624a5a6",
				"f6573119-4305-4af1-a99c-6286fa7876d0",
				"0859fb47-a805-40f0-917b-199425e80721",
		};
		try {
			generateAlbums(albumIds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<Album> getAllAlbums() {
		return  AlbumStorage.getList();
	}
	
	public Album getAlbum(long id) {
		Album album = new Album();
		//System.out.println(this.albumStorage.getAlbum(id));
		return album;
	}

	public void add(String mbid) {
		MusicBrainzQuery mbq = new MusicBrainzQuery(
				"music_catalogue",
				"0.1",
				"jukrsund@student.jyu.fi");
		String s = mbq.getAlbumById(mbid);
		Album a = MusicBrainzQueryResultConverter.convertJsonToAlbum(s);
		//a.setId(id);
		System.out.println(a.getArtist());
		//albums.put(id, a);
		//id++;
	}

	public Album createAlbums(String mbid) {
		// TODO Auto-generated method stub
		MusicBrainzQuery mbq = new MusicBrainzQuery(
				"music_catalogue",
				"0.1",
				"jukrsund@student.jyu.fi");
		String s = mbq.getAlbumById(mbid);
		Album a = MusicBrainzQueryResultConverter.convertJsonToAlbum(s);
		//a.setId(id);
		//albums.put(id, a);
		//this.albumStorage.add(a);
		//id++;
		return a;
	}

	public void removeAlbum(long id2) {
		// TODO Auto-generated method stub
		//this.albumStorage.removeAlbum(id2);
	}

	public Album updateAlbum(long id2, String title) {
		// TODO Auto-generated method stub
		Album album = new Album();
		return album;
	}

	/*
	public Album createReview(long id3, Review review) {
		Album album = this.albumStorage.createReview(id3, review);
		return album;
	}
	*/
	
	private void generateAlbums(String[] albumIds) throws InterruptedException {
		MusicBrainzQuery mbq = new MusicBrainzQuery("My Test App", "0.1", "jukrsund@student.jyu.fi");
		for (String s : albumIds) {
			System.out.println(s);
			String res = mbq.getAlbumById(s);
			Album a = MusicBrainzQueryResultConverter.convertJsonToAlbum(res);
			//a.setId(id);
			//albumStorage.add(a);
			//id++;
			TimeUnit.MILLISECONDS.sleep(1500);
		}
	}
}
