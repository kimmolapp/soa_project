package team1soa.music_catalogue;

import java.util.List;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.*;

import javax.inject.Singleton;
import javax.management.relation.RelationServiceNotRegisteredException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Response.Status;

import musicbrainz.MusicBrainzQuery;
import musicbrainz.MusicBrainzQueryResultConverter;
import team1soa.bean.ArtistJaxBean;
import team1soa.bean.ReviewJaxBean;
import team1soa.exception.DataNotFoundException;
import team1soa.resource.Album;
import team1soa.resource.Artist;
import team1soa.resource.Review;
import team1soa.storage.AlbumStorage;
import team1soa.storage.ArtistStorage;

@Singleton
@Path("artists")
public class ArtistResource {
	// private ArtistService artistService = new ArtistService();

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getartists(@Context UriInfo info) {
		Map<Long, Artist> artists = ArtistStorage.getAllArtists();
		if (artists == null) {
			throw new DataNotFoundException("List of artists is empty");
		}

		String nameQuery = info.getQueryParameters().getFirst("name");
		List<Artist> queryedArtists = new ArrayList<Artist>();
		Pattern p = Pattern.compile(".*" + nameQuery + ".*", Pattern.CASE_INSENSITIVE);
		if (nameQuery != null) {
			for (Artist artist : artists.values()) {
				if (p.matcher(artist.getName()).matches()) {
					queryedArtists.add(artist);
				}
			}
			return Response.status(Status.OK).entity(queryedArtists).build();

		}
		return Response.status(Status.OK).entity(artists).build();
	}

	@GET
	@Path("/{artistId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlbum(@PathParam("artistId") long id) {
		Artist artist = ArtistStorage.getArtist(id);
		if (artist == null) {
			throw new DataNotFoundException("Artist with id " + id + " not found");
		}
		return Response.status(Status.OK)
				.header(HttpHeaders.LOCATION, "http://localhost:8080/music_catalogue/webapi/artists/" + artist.getId())
				.entity(artist).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createartist(ArtistJaxBean input) {

		Artist artist = new Artist();
		if (input.mbid != null) {
			MusicBrainzQuery mbq = new MusicBrainzQuery("music_catalogue", "0.1", "jukrsund@student.jyu.fi");
			String s = mbq.getArtistById(input.mbid);
			artist = MusicBrainzQueryResultConverter.convertJsonToArtist(s);
			ArtistStorage.addArtist(artist);

			artist.addLink("/webapi/artists/" + artist.getId(), "self");
			return Response.status(Status.CREATED)
					.header(HttpHeaders.LOCATION,
							"http://localhost:8080/music_catalogue/webapi/albums/" + artist.getId())
					.entity(artist).build();
		} else if (input.name != null) {
			/*
			 * if (input.mbid != null) { artist =
			 * this.artistService.createartists(input.mbid);
			 * 
			 * artist.addLink("/webapi/artists/" + artist.getId(), "self"); return
			 * Response.status(Status.CREATED) .header(HttpHeaders.LOCATION,
			 * "http://localhost:8080/music_catalogue/webapi/artists/"+artist.getId())
			 * .entity(artist) .build(); } else if (input.artist != null && input.title !=
			 * null) {
			 */
			// TODO: create a new artist without mbid
			artist.setCountry(input.country);
			artist.setDisabiguation(input.disabiguation);
			artist.setName(input.name);
			// }
			ArtistStorage.addArtist(artist);

			artist.addLink("/webapi/artists/" + artist.getId(), "self");
			return Response.status(Status.CREATED)
					.header(HttpHeaders.LOCATION,
							"http://localhost:8080/music_catalogue/webapi/artists/" + artist.getId())
					.entity(artist).build();
		}
		return Response.status(Status.BAD_REQUEST).build();
	}

	@DELETE
	@Path("/{artistId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteArtist(@PathParam("artistId") long id) {
		if (ArtistStorage.getArtist(id) == null) {
			throw new DataNotFoundException("Artist with id " + id + " not found");
		}
		ArtistStorage.remove(id);
		return Response.status(Status.NO_CONTENT).build();
	}

	@PUT
	@Path("/{artistId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response createReview(ArtistJaxBean inputArtist, @PathParam("artistId") long id) {
		Artist artist = ArtistStorage.getArtist(id);
		if (artist == null) {
			throw new DataNotFoundException("Artist with id " + id + " not found");
		}
		if (inputArtist.country != null) {
			artist.setCountry(inputArtist.country);
		}
		if (inputArtist.disabiguation != null) {
			artist.setDisabiguation(inputArtist.disabiguation);
		}
		if (inputArtist.name != null) {
			artist.setName(inputArtist.name);
		}

		return Response.status(Status.NO_CONTENT).entity(artist).build();
	}
}
