package team1soa.music_catalogue;

import java.util.Map;

import javax.inject.Singleton;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import team1soa.bean.AlbumJaxBean;
import team1soa.bean.ReviewJaxBean;
import team1soa.exception.DataNotFoundException;
import team1soa.resource.Album;
import team1soa.resource.Review;
import team1soa.storage.AlbumStorage;
import team1soa.bean.ReviewJaxBean;
import team1soa.exception.DataNotFoundException;
import team1soa.resource.Album;
import team1soa.resource.Review;

@Path("albums/{albumId}/review")
public class ReviewResource {
	

	@GET    
    @Produces(MediaType.APPLICATION_JSON)
    public Response getReview(@PathParam("albumId") long id){
    	Album album = AlbumStorage.get(id);
    	if (album == null) {
    		throw new DataNotFoundException("Album with id "+id+" not found"); 
    	}
    	return Response.status(Status.OK)
    			.header(HttpHeaders.LOCATION, "http://localhost:8080/music_catalogue/webapi/albums/"+album.getId())
    			.entity(album.getReview())
    			.build();
    }

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAlbum(@PathParam("albumId") int id, ReviewJaxBean input) {
    Review r = new Review();
    try {
    r.setMyRating(input.rating); 
    } catch (IllegalArgumentException e) {
    	return Response.status(Status.BAD_REQUEST)
        		.entity("Bad value of rating")
        		.build();
    }
    r.setMyReview(input.review);
    AlbumStorage.get(id).setReview(r);  
    return Response.status(Status.OK)
    		.entity(r)
    		.build();
}

}
