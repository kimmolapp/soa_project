package team1soa.resource;

public class Review {
	
	public Review() {
	}
	
	private int MyRating;
	private String MyReview;
	
	public int getMyRating() {
		return MyRating;
	}

	public void setMyRating(int MyRating) throws IllegalArgumentException {
		if (MyRating < 0 || MyRating > 5) {
			throw new IllegalArgumentException("Rating must be between 0-5!");
		} 
		this.MyRating = MyRating;
	} 
	
	public String getMyReview() {
		return MyReview;
	}

	public void setMyReview(String MyReview) {
		this.MyReview = MyReview;
	}

}