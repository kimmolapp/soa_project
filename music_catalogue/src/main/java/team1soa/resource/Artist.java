package team1soa.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import team1soa.music_catalogue.Link;

@XmlRootElement
public class Artist {
	public String getArtist_mbid() {
		return artist_mbid;
	}

	public void setArtist_mbid(String artist_mbid) {
		this.artist_mbid = artist_mbid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDisabiguation() {
		return disabiguation;
	}

	public void setDisabiguation(String disabiguation) {
		this.disabiguation = disabiguation;
	}

	private String artist_mbid;
	private String name;
	private String country;
	private String disabiguation;
	private long id;

	private List<Link> links = new ArrayList<>();
	private List<Event> events = new ArrayList<>();

	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public void addLink(String url, String rel) {
		Link link = new Link();
		link.setLink(url);
		link.setRel(rel);
		links.add(link);
	}
	
	public List<Link> getLinks() {
		return links;
	}

	public void setLinks(List<Link> links) {
		this.links = links;
	}

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Artist(String artist_mbid) {}

	public Artist() {
	}
}
