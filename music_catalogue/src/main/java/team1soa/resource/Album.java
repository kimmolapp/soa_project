package team1soa.resource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import team1soa.music_catalogue.Link;

@XmlRootElement
public class Album {
	private long id;
	private String title;
	private Review review; 
	private String artist; // TODO: create Artist object for this
	private Long artist_id;
	private String date; // TODO: Change this to Date format
	private String mbid;
	private String label;
	private String format;
	private String type;
	private String artist_mbid;
	
	private List<Track> tracks = new ArrayList<>();
	private List<Link> links = new ArrayList<>();
	
	
	public List<Link> getLinks() {
		return links;
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Review getReview() {
		return review;
	}

	public void setReview(Review review) {
		this.review = review;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMbid() {
		return mbid;
	}

	public void setMbid(String mbid) {
		this.mbid = mbid;
	}

	public List<Track> getTracks() {
		return tracks;
	}

	public void setTracks(List<Track> tracks) {
		this.tracks = tracks;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}
	
	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getArtist_mbid() {
		return artist_mbid;
	}


	public void setArtist_mbid(String artist_mbid) {
		this.artist_mbid = artist_mbid;
	}


	public void addLink(String url, String rel) {
		Link link = new Link();
		link.setLink(url);
		link.setRel(rel);
		links.add(link);
	}
	
	@Override
	public String toString()
	{
		return this.id+" "+this.getTitle()+", "+this.getArtist()+" ";
	}


	public Long getArtist_id() {
		return artist_id;
	}


	public void setArtist_id(Long artist_id) {
		this.artist_id = artist_id;
	}
	
}
