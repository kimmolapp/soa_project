package musicbrainz;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import team1soa.resource.Album;
import team1soa.resource.Artist;
import team1soa.resource.Track;

/**
 * Provides methods that convert MusicBrainz search results into POJOs used
 * in music_catalogue application.
 *  
 * @author jussi
 * 
 * TODO: test the converter with some bad inputs to determine which type of
 * 			errors should be thrown.
 */
public class MusicBrainzQueryResultConverter {
	
	/**
	 * Creates a new Album object and parses it's information from a String. String
	 * must be in Json format.
	 * @param jsonString json formatted String that contains album information.
	 * @return new Album object
	 */
	public static Album convertJsonToAlbum(final String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject albumJson = parser.parse(jsonString).getAsJsonObject();
		
		return convertJsonToAlbum(albumJson);
	}
	
	/**
	 * Creates a new Album object and parses it's information from a JsonObject.
	 * @param albumJson	JsonObject containing album information
	 * @return new Album object
	 */
	public static Album convertJsonToAlbum(final JsonObject albumJson) {
		Album album = new Album();
		
		// Get artist info from artist-credit
		JsonArray artists = albumJson.getAsJsonArray("artist-credit");
		if (artists.size() > 1)
			album.setArtist("Various artists");
		else {
			JsonObject artist = artists.get(0).getAsJsonObject().getAsJsonObject("artist");
			album.setArtist(artist.get("name").getAsString());
			album.setArtist_mbid(artist.get("id").getAsString());
		}
		
		album.setTitle(albumJson.get("title").getAsString());
		album.setDate(albumJson.get("date").getAsString());
		album.setMbid(albumJson.get("id").getAsString());
		
		// Get record label from label
		JsonObject label = albumJson.get("label-info").getAsJsonArray().get(0).getAsJsonObject().getAsJsonObject("label");
		album.setLabel(label.get("name").getAsString());

		// Get type from release group
		JsonObject releaseGroup = albumJson.getAsJsonObject("release-group");
		System.out.println(releaseGroup);
		album.setType(releaseGroup.get("primary-type").getAsString());
		
		// Get track listing and format from media
		JsonArray discs = albumJson.getAsJsonArray("media");
		List<Track> tracks = new ArrayList<Track>();
		int discNumber = 1;		
		
		for (JsonElement discElem : discs) {
			JsonObject disc = discElem.getAsJsonObject();
			album.setFormat(disc.get("format").getAsString());
			if (!disc.has("tracks"))
				continue;
			JsonArray discTracks = disc.getAsJsonArray("tracks");
			
			for (JsonElement trackElem : discTracks) {
				JsonObject jsonTrack = trackElem.getAsJsonObject();
				JsonArray trackArtistArr = jsonTrack.getAsJsonArray("artist-credit");				
				List<String> trackArtistList = new ArrayList<String>();
				for (JsonElement artistElem : trackArtistArr) {
					JsonObject artist = artistElem.getAsJsonObject();
					trackArtistList.add(artist.get("name").getAsString());
				}
				
				Track track = new Track();
				track.setArtist(String.join(", ", trackArtistList));
				track.setTitle(jsonTrack.get("title").getAsString());
				track.setNumber(String.format("%d.%s", discNumber, jsonTrack.get("number").getAsString()));
				try {
					track.setLength(jsonTrack.get("length").getAsInt());
				} catch (UnsupportedOperationException e) {
					// invalid value for time
				}
				tracks.add(track);
			}
			
			discNumber++;					
		}
		album.setTracks(tracks);
		
		return album;
	}
	
	public static Artist convertJsonToArtist(final String jsonString) {
		JsonParser parser = new JsonParser();
		JsonObject artistJson = parser.parse(jsonString).getAsJsonObject();
		
		return convertJsonToArtist(artistJson);
	}
	
	public static Artist convertJsonToArtist(final JsonObject artistJson) {
		Artist artist = new Artist();
		artist.setName(artistJson.get("name").getAsString());
		artist.setArtist_mbid(artistJson.get("id").getAsString());
		artist.setDisabiguation(artistJson.get("disambiguation").getAsString());

		//if (!artistJson.get("type").isJsonNull())
		//	artist.setType(artistJson.get("type").getAsString());
		if (!artistJson.get("country").isJsonNull())
			artist.setCountry(artistJson.get("country").getAsString());
		
		return artist;
	}
}