package musicbrainz;

public enum ResultFormat {
	JSON, XML
}