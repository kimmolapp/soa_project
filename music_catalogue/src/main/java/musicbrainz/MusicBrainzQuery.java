package musicbrainz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Provides methods for querying the MusicBrainz database.
 * 
 * TODO: Get track listing when doing queries
 * 
 * @author jussi
 */
public class MusicBrainzQuery {
	private static final String API_URL = "http://musicbrainz.org/ws/2";
	private String appName;
	private String version;
	private String contact;
	private ResultFormat resultFormat = ResultFormat.JSON; // use json as default

	/**
	 * Creates new MusicBrainzQuery object that can search MusicBrainz database by
	 * calling the Web Service API. Callers must identify themselves by providing 
	 * the name and version of their application as well as contact information.
	 * 
	 * @param appName
	 *            Name of the application
	 * @param version
	 *            Version of the application
	 * @param contact
	 *            E-mail or URL
	 */
	public MusicBrainzQuery(String appName, String version, String contact) {
		this.appName = appName;
		this.version = version;
		this.contact = contact;
	}

	/**
	 * Returns the information of an artist specified by the id.
	 * 
	 * @param artistId	artist's id in MusicBrainz
	 * @return String in either xml or json format (json by default)
	 */
	public String getArtistById(String artistId) {
		return doLookup("artist", artistId, "");
	}
	
	/**
	 * Returns the information of an album specified by the id.
	 * 
	 * @param albumId	album's id in MusicBrainz
	 * @return String in either xml or json format (json by default)
	 */
	public String getAlbumById(String albumId) {
		return doLookup("release", albumId, "artist-credits+recordings+labels+release-groups");
	}
	
	/**
	 * Queries MusicBrainz to find albums that have the specified barcode.
	 * @param barcode	barcode of the album
	 * @return String 
	 */
	public String getAlbumsByBarcode(long barcode) {
		return doQuery("release", "barcode:" + barcode, "artist-credits+recordings+labels");
	}
	
	/**
	 * Looks up an entity using the entity's MusicBrainz id.
	 * 
	 * @param entity 	for example artist, release, release-group etc.
	 * @param id		MusicBrainz id for the entity
	 * @param inc		extra fields to be included in the results
	 * @return String representing the entity in either xml or json format
	 */
	public String doLookup(String entity, String id, String inc) {
		String paramString = String.format("%s/%s", entity, id);
		paramString += inc.equals("") ? "" : "?inc=" + inc;
		return doGETRequest(paramString);
	}
	
	/**
	 * Queries the MusicBrainz database.
	 * 
	 * @param entity	determines which entities to query (for example
	 * 							artist, release, release-group)
	 * @param params	parameters of the query
	 * @param inc		extra fields to be included in the results
	 * @return String representing a list of query results in either xml
	 * 							or json format. 
	 */
	public String doQuery(String entity, String params, String inc) {
		String paramString = String.format("%s?query=%s", entity, params);
		paramString += inc.equals("") ? "" : "&inc=" + inc; 
		return doGETRequest(paramString);
	}
	
	/**
	 * Makes a GET request to the MusicBrainz Web Service.
	 * @param paramString	path and parameters of the GET request
	 * @return service response as a String
	 */
	public String doGETRequest(String paramString) {
		String urlString = String.format("%s/%s", API_URL, paramString);
		HttpURLConnection connection = null;
		System.out.println(urlString);
		try {
			URL url = new URL(urlString);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("User-Agent", getUserAgent());
			connection.setRequestProperty("Accept", getAcceptHeader());

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			return response.toString();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/**
	 * Returns the value to be used in the User-Agent header.
	 */
	public String getUserAgent() {
		return String.format("%s/%s { %s }", appName, version, contact);
	}

	/**
	 * Returns the selected result format (either ResultFormat.XML or ResultFormat.JSON).
	 */
	public ResultFormat getResultFormat() {
		return resultFormat;
	}

	/**
	 * Sets the format in which the results of the query should be returned.
	 * @param resultFormat	either ResultFormat.XML or ResultFormat.JSON (default)
	 */
	public void setResultFormat(ResultFormat resultFormat) {
		this.resultFormat = resultFormat;
	}

	/**
	 * Returns the value to be used in the Accept header.
	 * @return either "application/json" or "application/xml"
	 */
	public String getAcceptHeader() {
		String contentType = "";
		switch (resultFormat) {
		case JSON:
			contentType = "application/json";
			break;
		case XML:
			contentType = "application/xml";
			break;
		}
		return contentType;
	}
}