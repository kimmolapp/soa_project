package musicbrainz;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import team1soa.resource.Album;
import team1soa.resource.Artist;
import team1soa.resource.Track;

public class Main {
	private static String[] albumIds = {
			"5d227193-8f7e-4f6b-b9d1-2faa3624a5a6",
			"f6573119-4305-4af1-a99c-6286fa7876d0",
			"0859fb47-a805-40f0-917b-199425e80721",
			"927e2755-860d-47aa-8b75-bfd600c7040f",
			"a35f18e1-1435-4eb8-b344-ed1e88218436",
			"64d16be8-0039-4c74-86ed-4d16b61ed59b",
			"b4fffcb0-4723-4c91-8e09-bd76876d7e9a",
			"b3b992ab-9313-31f0-91ea-e1b8599830c5",
			"5be001e7-f75e-344c-8973-8c5fb1753caf",
			"1b105601-d2d3-4da2-a7d9-114f981b1766",
			"8094543e-de11-4f33-9c10-fb90056210a9",
			"f6010e25-d302-4848-9b07-53f715c363bb",
			"2c244bca-f6b0-408c-aa9c-0e3a693d8f5e",
			"6136bc33-1e9c-4e76-8c9f-2c71fcf1c82b"
	};
	private static String[] artistIds = {
			"5b11f4ce-a62d-471e-81fc-a69a8278c7da",
			"ed46b78e-dae1-4534-bc91-341066f35599",
			"ce6b102e-63f9-4618-a9cb-9731b5e37c4b",
			"2b798105-7746-4d2a-bbe4-531859bfa1a8",
			"f26c72d3-e52c-467b-b651-679c73d8e1a7"
	};
	private static Long[] barcodes = {
			853895007018L,
			6418547015533L,
			074646574720L,
			5099765157123L
	};
	

	/**
	 * Use this to test the API. Keep it to one request per second.
	 * 
	 * Example url:
	 * http://musicbrainz.org/ws/2/release/f6573119-4305-4af1-a99c-6286fa7876d0?inc=artist-credits+recordings+labels
	 * 
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {	
		Album a;
		MusicBrainzQuery mbq = new MusicBrainzQuery("My Test App", "0.1", "jukrsund@student.jyu.fi");

		albumIdTest(albumIds);
		//artistIdTest(artistIds);
		//barcodeSearchTest(barcodes);
		System.out.println("DONE");
		
	}
	
	private static void albumIdTest(String[] albumIds) throws InterruptedException {
		Album a;
		MusicBrainzQuery mbq = new MusicBrainzQuery("My Test App", "0.1", "jukrsund@student.jyu.fi");
		
		JsonArray arr = new JsonArray();
		for (String s : albumIds) {
			System.out.println(s); 
			String res = mbq.getAlbumById(s);

			JsonParser parser = new JsonParser();
			JsonObject json = parser.parse(res).getAsJsonObject();
			arr.add(json);
			
			
			a = MusicBrainzQueryResultConverter.convertJsonToAlbum(res);
			printAlbum(a);
			TimeUnit.MILLISECONDS.sleep(1500);
		}

		writeToFile("albums.txt", arr.toString());
	}
	
	private static void artistIdTest(String[] artistIds) throws InterruptedException {
		Artist a;
		MusicBrainzQuery mbq = new MusicBrainzQuery("My Test App", "0.1", "jukrsund@student.jyu.fi");
		for (String s : artistIds) {
			System.out.println(s);
			String res = mbq.getArtistById(s);
			
			
			a = MusicBrainzQueryResultConverter.convertJsonToArtist(res);
			printArtist(a);
			TimeUnit.MILLISECONDS.sleep(1500);
		}
	}
	
	private static void printArtist(Artist a) {
		System.out.println("==========ARTIST=========");
		System.out.println("name: " + a.getName());
		System.out.println("mbid: " + a.getArtist_mbid());
		//System.out.println("type: " + a.getType());
		System.out.println("country: " + a.getCountry());
		System.out.println("desc: " + a.getDisabiguation());
		System.out.println("========================");
	}

	private static void barcodeSearchTest(Long[] barcodes) throws InterruptedException {
		Album a;
		MusicBrainzQuery mbq = new MusicBrainzQuery("My Test App", "0.1", "jukrsund@student.jyu.fi");
		for (Long barcode : barcodes) {
			System.out.println(barcode);
			String res = mbq.getAlbumsByBarcode(barcode);
			JsonParser parser = new JsonParser();
			JsonArray arr = parser.parse(res).getAsJsonObject().get("releases").getAsJsonArray();
			System.out.println("Found " + arr.size());
			for (JsonElement elem : arr) {
				a = MusicBrainzQueryResultConverter.convertJsonToAlbum(elem.getAsJsonObject());
				printAlbum(a);
			}
			TimeUnit.MILLISECONDS.sleep(1500);
		}
	}
	
	private static void printAlbum(Album a) {
		System.out.println("==========ALBUM=========");
		System.out.println("artist: " + a.getArtist());
		System.out.println("artist_mbid: " + a.getArtist_mbid());
		System.out.println("title: " + a.getTitle());
		System.out.println("date: " + a.getDate());
		System.out.println("label: " + a.getLabel());
		System.out.println("format: " + a.getFormat());
		System.out.println("mbid: " + a.getMbid());
		System.out.println("type: " + a.getType());
		System.out.println("number of tracks: " + a.getTracks().size());
		for (Track t : a.getTracks())
			System.out.println(String.format("%s %s %s %s", t.getArtist(), t.getNumber(), t.getTitle(), t.getLength()));
		System.out.println("========================");
	}
	
	private static void writeToFile(String file, String s) {
		try(FileWriter fw = new FileWriter(file, true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw)) {
			    out.println(s);
			} catch (IOException e) {
				System.out.println("exception");
			}
	}
}