// Creates an ajax request using jQuery.
function doAjaxRequest(url, params, contentType, method) {
	params = params ? params : "";

	console.log("=== Request ===");
	console.log(url, params, contentType, method);

	$.ajax({
		beforeSend : function(jqXHR, settings) {
			$("#loading").show();
		},
		url : url,
		contentType : contentType,
		data : params,
		cache : false,
		method : method,
		complete : function(xhr, message) {
			$("#loading").hide();
			$("#status").text(xhr["status"]);
			var output = $("#output");
			output.empty();
			try {
				// Try to parse the response into json object and pretty
				// print it on the client
				var response = JSON.parse(xhr["responseText"]);
				var text = JSON.stringify(response, undefined, 4);
				var code = $("<code>");
				code.html(syntaxHighlight(text));
				output.append(code);
			}
			catch(e) {
				// The response text is in html so create a new iframe
				// to display it
				var iframe = $("<iframe>");
				iframe.attr("srcdoc", xhr["responseText"]);
				output.append(iframe);
			}
		}
	});
}

// Returns the data of a form as a json object.
function getFormData($form) {
	var formArr = $form.serializeArray();
	var formJson = {};
	$.map(formArr, function(n, i) {
		if (n["value"])
			formJson[n["name"]] = n["value"];
	});
	return formJson;
}

// Function to highlight json syntax. From:
// https://stackoverflow.com/questions/4810841/how-can-i-pretty-print-json-using-javascript#7220510
function syntaxHighlight(json) {
    if (typeof json != 'string') {
         json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}