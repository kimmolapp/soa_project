package servlets;

import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import config.AppConfig;

/**
 * Servlet implementation class DBServlet
 */
@WebServlet("/DBServlet")
public class DBServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CONTENT_TYPE = "text/html";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DBServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		response.setContentType(CONTENT_TYPE);
		String payload = "";
		try {
			payload = sendRequest(payload);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		try ( PrintWriter writer = response.getWriter() ) {
			writer.write(payload);			
		} 
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				
		String reqType = request.getParameter("reqType");
		switch (reqType) {
		
		case "tokeni":
			String value = request.getParameter("code");			
			response.setContentType(CONTENT_TYPE);
			JsonObject jso = new JsonObject();			
			try {
				jso = getAccessToken(value);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setContentType("application/json;charset=UTF-8");			
			try ( PrintWriter writer = response.getWriter() ) {			
				writer.write(jso.toString());			
			} 
			break;
		
		case "userInfo":
			JsonObject jso2 = new JsonObject();
			String access_token = request.getParameter("access_token");
			String accountId = request.getParameter("account_id");
			try {
				jso2 = getAccountInfo(access_token, accountId);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.setContentType("application/json;charset=UTF-8");			
			try ( PrintWriter writer = response.getWriter() ) {			
				writer.write(jso2.toString());			
			} 
			break;
			
		case "uploadPic":
			String access_token2 = request.getParameter("access_token");
			String path = request.getParameter("path");
			try {
				uploadFile(access_token2, path);
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		break;
		
		default:
			break;
		
		}		
	}

	public String sendRequest(String str) throws URISyntaxException, IOException {		
		String appKey = AppConfig.APP_KEY;
		String redirectURI="http://localhost:8080/MyDBoxClient/"; //any url to where you want to redirect the user
		URI uri=new URI("https://www.dropbox.com/oauth2/authorize");
		StringBuilder requestUri=new StringBuilder(uri.toString());
		requestUri.append("?client_id=");
		requestUri.append(URLEncoder.encode(appKey,"UTF-8"));
		requestUri.append("&response_type=code");
		requestUri.append("&redirect_uri="+redirectURI.toString());
		String queryResult = requestUri.toString();
		return queryResult;
	}
	
	public JsonObject getAccessToken(String codeStr) throws URISyntaxException, IOException {
		
		JsonParser parser = new JsonParser();
		JsonObject o = new JsonObject();
		
		String code = ""+codeStr; //code get from previous step
		
		String appKey = AppConfig.APP_KEY;
		String appSecret = AppConfig.APP_SECRET_KEY;
		String redirectURI="http://localhost:8080/MyDBoxClient/"; //any url to where you want to redirect the user
		StringBuilder tokenUri=new StringBuilder("code=");
		tokenUri.append(URLEncoder.encode(code,"UTF-8"));
		tokenUri.append("&grant_type=");
		tokenUri.append(URLEncoder.encode("authorization_code","UTF-8"));
		tokenUri.append("&client_id=");
		tokenUri.append(URLEncoder.encode(appKey,"UTF-8"));
		tokenUri.append("&client_secret=");
		tokenUri.append(URLEncoder.encode(appSecret,"UTF-8"));
		tokenUri.append("&redirect_uri="+redirectURI);
		URL url=new URL("https://api.dropbox.com/oauth2/token");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		connection.setRequestProperty("Content-Length", "" + tokenUri.toString().length());
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
		outputStreamWriter.write(tokenUri.toString());
		outputStreamWriter.flush();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null) {			
			o = parser.parse(inputLine).getAsJsonObject();			
		}
		in.close();
				
		} finally {
		connection.disconnect();
		}
		return o;
	}
	
	public JsonObject getAccountInfo(String tokenStr, String accountIDStr) throws URISyntaxException, IOException {
		JsonParser parser = new JsonParser();
		JsonObject o = new JsonObject();
		String access_token = ""+tokenStr;
		String content = "{\"account_id\": \"" + accountIDStr + "\"}";
		System.out.println(content);
		URL url = new URL("https://api.dropboxapi.com/2/users/get_account");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Authorization", "Bearer "+access_token);
		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("Content-Length", "" + content.length());
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
		outputStreamWriter.write(content);
		outputStreamWriter.flush();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;		
		while ((inputLine = in.readLine()) != null) {
			o = parser.parse(inputLine).getAsJsonObject();			
		}
		in.close();		
		} finally {
		connection.disconnect();
		}
		return o;
		}
	
	public void uploadFile(String token, String path) throws URISyntaxException, IOException {
		String access_token = ""+token;
		String sourcePath = ""+path; //required file path on local file system
		Path pathFile = Paths.get(sourcePath);
		byte[] data = Files.readAllBytes(pathFile);
		String content = "{\"path\": \"/pics/dropb.png\",\"mode\": \"add\",\"autorename\": true,\"mute\": false,\"strict_conflict\": false}";
		URL url = new URL("https://content.dropboxapi.com/2/files/upload");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
		connection.setDoOutput(true);
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Authorization", "Bearer "+access_token);
		connection.setRequestProperty("Content-Type", "application/octet-stream");
		connection.setRequestProperty("Dropbox-API-Arg", "" + content);
		connection.setRequestProperty("Content-Length", String.valueOf(data.length));
		OutputStream outputStream = connection.getOutputStream();
		outputStream.write(data);
		outputStream.flush();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) { 
			System.out.println(inputLine);
			response.append(inputLine); 
			}
		in.close();
		
		} finally { connection.disconnect(); }
	}
}
