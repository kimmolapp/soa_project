package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

/**
 * Servlet implementation class Files
 */
@WebServlet("/files")
public class Files extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Files() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String access_token = request.getParameter("access_token");
		URL url = new URL("https://api.dropboxapi.com/2/files/list_folder");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		String content = "{\"path\": \"/pics/\"}";

		JsonArray files = new JsonArray();
		JsonParser parser = new JsonParser();

		try {
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Host", "https://api.dropboxapi.com");
			connection.setRequestProperty("User-Agent", "api-explorer-client");
			connection.setRequestProperty("Authorization", "Bearer " + access_token);
			connection.setRequestProperty("Content-Type", "application/json");

			System.out.println(connection.toString());
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			outputStreamWriter.write(content);
			outputStreamWriter.flush();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
				files.add(parser.parse(inputLine).getAsJsonObject());
			}
			in.close();
		} finally {
			connection.disconnect();
		}
		
		response.setContentType("application/json;charset=UTF-8");			
		try ( PrintWriter writer = response.getWriter() ) {			
			writer.write(files.toString());			
		} 
	}
}

