package servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;


/**
 * Servlet implementation class fileUpload
 */
@WebServlet("/fileUpload")
@MultipartConfig
public class FileUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FileUpload() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
		
		// Check first that the contentType is multidata/form-data -format		
		String contentType = request.getContentType();
		if (!contentType.startsWith("multi")) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		
		// Get the inputStream of the selected file
		Part filePart = request.getPart("file");
		String fileName = filePart.getSubmittedFileName();		
		InputStream fileContent = filePart.getInputStream();
		
		String account_token = request.getParameter("account_token");
		
		// Try to upload the file and send a proper status to the client.
		try {
			uploadFile(account_token, fileContent, fileName);
			response.setStatus(HttpServletResponse.SC_OK);
		} catch (URISyntaxException e1) {
			e1.printStackTrace();			
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (IOException e2) {
			e2.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	/**
	 * Uploads the file in the specified file path to Dropbox using the account token.
	 * @param token account token identifying the user's account	
	 * @param fileContent	InputStream of the selected file
	 * @param fileName	name of the file when it is uploaded to Dropbox
	 * @throws URISyntaxException
	 * @throws IOException
	 */
	private void uploadFile(String token, InputStream fileContent, String fileName) throws URISyntaxException, IOException {
		String access_token = "" + token;
		
		String content = "{\"path\": \"/pics/"+fileName+"\",\"mode\": \"add\",\"autorename\": true,\"mute\": false,\"strict_conflict\": false}";
		URL url = new URL("https://content.dropboxapi.com/2/files/upload");

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		
		try {
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", "Bearer " + access_token);
			connection.setRequestProperty("Content-Type", "application/octet-stream");
			connection.setRequestProperty("Dropbox-API-Arg", "" + content);

			OutputStream outputStream = connection.getOutputStream();
			IOUtils.copy(fileContent, outputStream);
			outputStream.flush();
			outputStream.close();
			fileContent.close();

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
				response.append(inputLine);
			}
			in.close();

		} finally {
			connection.disconnect();
		}

	}

}
