var koodi = window.location.href.substring(41);
var tokenJson = "";

function konnekti() {   
    var params ="reqType=konnekti";
    doAjaxRequest("DBServlet", params, "GET", konnekti_back);
}

function konnekti_back(response) {
        window.location = (response);
}

function tokeni() { 
    var params = "reqType=tokeni";
    params += "&code="+koodi;
    doAjaxRequest("DBServlet", params, "post", tokeni_back);
}

function tokeni_back(result)
{   
    console.log(result);
    var divi = document.getElementById("divi2");
    var UusiDiv = document.createElement("div");
    
    var content = document.createTextNode("Access token received"); 
    divi.appendChild(content);
    tokenJson = result; 
}

function userInfo() {
    var params = "reqType=userInfo";
    params += "&access_token=" + tokenJson["access_token"];
    params += "&account_id=" + tokenJson["account_id"];
    
    doAjaxRequest("DBServlet", params, "post", userInfo_back);

}

function userInfo_back(result) {
    var divi = document.getElementById("divi3");
    var UusiDiv = document.createElement("div");
    console.log(result);
    var content = document.createTextNode(`Hello ${result["name"].given_name}!`);   
    divi.appendChild(content);
    
}

function uploadPic() {
    var params = "reqType=uploadPic";
    params += "&access_token=" + tokenJson["access_token"];
    params += "&path=D:/11s/finland/soa/task2/soa_tasks/MyDBoxClient/MyDBoxClient/WebContent/pics/dropb.png";
    doAjaxRequest("DBServlet", params, "post", uploadPic_back);
}

function uploadPic_back() {
    
}
// Creates an ajax request using jQuery.
function doAjaxRequest(url, params, method, backF) {    
    console.log("=== Request ==="); 
    console.log(url, params, method);

    $.ajax({
        beforeSend: function(jqXHR, settings) {
            $("#loading").show();
        },
        url: url,
        contentType: "application/x-www-form-urlencoded",
        data: params,
        cache: false,
        method: method,
        success: function(response) {
            $("#errors").hide();
            backF(response);
        },
        error: function(xhr, status, err) { 
            $("#errors").text("Something went wrong. Error status: " + xhr.status);
            $("#errors").show();
            console.log(xhr);
        },
        complete: function(xhr, message) {
            $("#loading").hide();
        }
    });
}

