<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="sampleCalculatorServiceImplProxyid" scope="session" class="service.CalculatorServiceImplProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
sampleCalculatorServiceImplProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = sampleCalculatorServiceImplProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        sampleCalculatorServiceImplProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        service.CalculatorServiceImpl getCalculatorServiceImpl10mtemp = sampleCalculatorServiceImplProxyid.getCalculatorServiceImpl();
if(getCalculatorServiceImpl10mtemp == null){
%>
<%=getCalculatorServiceImpl10mtemp %>
<%
}else{
        if(getCalculatorServiceImpl10mtemp!= null){
        String tempreturnp11 = getCalculatorServiceImpl10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String a_1id=  request.getParameter("a16");
        double a_1idTemp  = Double.parseDouble(a_1id);
        String b_2id=  request.getParameter("b18");
        double b_2idTemp  = Double.parseDouble(b_2id);
        double add13mtemp = sampleCalculatorServiceImplProxyid.add(a_1idTemp,b_2idTemp);
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(add13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
break;
case 20:
        gotMethod = true;
        String a_3id=  request.getParameter("a23");
        double a_3idTemp  = Double.parseDouble(a_3id);
        String b_4id=  request.getParameter("b25");
        double b_4idTemp  = Double.parseDouble(b_4id);
        double divide20mtemp = sampleCalculatorServiceImplProxyid.divide(a_3idTemp,b_4idTemp);
        String tempResultreturnp21 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(divide20mtemp));
        %>
        <%= tempResultreturnp21 %>
        <%
break;
case 27:
        gotMethod = true;
        String startSum_5id=  request.getParameter("startSum30");
        double startSum_5idTemp  = Double.parseDouble(startSum_5id);
        String interest_6id=  request.getParameter("interest32");
        double interest_6idTemp  = Double.parseDouble(interest_6id);
        String years_7id=  request.getParameter("years34");
        int years_7idTemp  = Integer.parseInt(years_7id);
        double interest27mtemp = sampleCalculatorServiceImplProxyid.interest(startSum_5idTemp,interest_6idTemp,years_7idTemp);
        String tempResultreturnp28 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(interest27mtemp));
        %>
        <%= tempResultreturnp28 %>
        <%
break;
case 36:
        gotMethod = true;
        String a_8id=  request.getParameter("a39");
        double a_8idTemp  = Double.parseDouble(a_8id);
        String b_9id=  request.getParameter("b41");
        double b_9idTemp  = Double.parseDouble(b_9id);
        double subtract36mtemp = sampleCalculatorServiceImplProxyid.subtract(a_8idTemp,b_9idTemp);
        String tempResultreturnp37 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(subtract36mtemp));
        %>
        <%= tempResultreturnp37 %>
        <%
break;
case 43:
        gotMethod = true;
        String a_10id=  request.getParameter("a46");
        double a_10idTemp  = Double.parseDouble(a_10id);
        String b_11id=  request.getParameter("b48");
        double b_11idTemp  = Double.parseDouble(b_11id);
        double multiply43mtemp = sampleCalculatorServiceImplProxyid.multiply(a_10idTemp,b_11idTemp);
        String tempResultreturnp44 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(multiply43mtemp));
        %>
        <%= tempResultreturnp44 %>
        <%
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>