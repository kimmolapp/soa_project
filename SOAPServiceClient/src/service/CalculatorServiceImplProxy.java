package service;

public class CalculatorServiceImplProxy implements service.CalculatorServiceImpl {
  private String _endpoint = null;
  private service.CalculatorServiceImpl calculatorServiceImpl = null;
  
  public CalculatorServiceImplProxy() {
    _initCalculatorServiceImplProxy();
  }
  
  public CalculatorServiceImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initCalculatorServiceImplProxy();
  }
  
  private void _initCalculatorServiceImplProxy() {
    try {
      calculatorServiceImpl = (new service.CalculatorServiceImplServiceLocator()).getCalculatorServiceImpl();
      if (calculatorServiceImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)calculatorServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)calculatorServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (calculatorServiceImpl != null)
      ((javax.xml.rpc.Stub)calculatorServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public service.CalculatorServiceImpl getCalculatorServiceImpl() {
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl;
  }
  
  public double add(double a, double b) throws java.rmi.RemoteException{
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl.add(a, b);
  }
  
  public double divide(double a, double b) throws java.rmi.RemoteException{
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl.divide(a, b);
  }
  
  public double interest(double startSum, double interest, int years) throws java.rmi.RemoteException{
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl.interest(startSum, interest, years);
  }
  
  public double subtract(double a, double b) throws java.rmi.RemoteException{
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl.subtract(a, b);
  }
  
  public double multiply(double a, double b) throws java.rmi.RemoteException{
    if (calculatorServiceImpl == null)
      _initCalculatorServiceImplProxy();
    return calculatorServiceImpl.multiply(a, b);
  }
  
  
}