package service;

public class CalculatorServiceImpl implements CalculatorService {

	@Override
	public double add(double a, double b) {
		return a + b;
	}

	@Override
	public double subtract(double a, double b) {
		return a - b;
	}

	@Override
	public double multiply(double a, double b) {
		return a * b;
	}

	@Override
	public double divide(double a, double b) {
		return (0 != b) ? a / b : 0;
	}

	@Override
	public double interest(double startSum, double interest, int years) {
		double answer = startSum;
		
		if (years < 0) { 
			interest = (1.0 / (1.0 + (interest / 100.0)) - 1) * 100;
			years = - years;
		}
		
		for (int i = 0; i < years; i++ ) {
			answer += answer * (interest / 100.0);
		}
		
		return answer;
	}

}
